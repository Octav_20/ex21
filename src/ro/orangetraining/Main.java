package ro.orangetraining;


import java.util.ArrayList;
import java.util.List;
import java.util.*;
import java.util.Comparator;


public class Main {

    public static void main(String[] args) {
        // write your code here
        List<Employee> lst = new ArrayList<>();
        Employee e1 = new Employee("Jon Age", 22, 1001);
        lst.add(e1);
        Employee e2 = new Employee("Steve Age", 19, 1003);
        lst.add(e2);
        Employee e3 = new Employee("Kevin Age", 23, 1005);
        lst.add(e3);
        Employee e4 = new Employee("Ron Age", 20, 1010);
        lst.add(e4);
        Employee e5 = new Employee("Lucy Age", 18, 1111);
        lst.add(e5);
        lst.forEach(System.out::print);
        //System.out.print("Original list: \n"+ e1.toString()+e2.toString()+e3.toString()+e4.toString()+e5.toString());
        //Comparator<Employee> comparator=
        //sort by age
        Comparator<Employee> byAge=Comparator.comparing(Employee::getAge,Comparator.naturalOrder());
        Comparator<Employee> byName=Comparator.comparing(Employee::getName,Comparator.naturalOrder());
        Comparator<Employee> byId=Comparator.comparing(Employee::getId,Comparator.naturalOrder());
        lst.sort(byAge);
        System.out.println("By age:  ");
        lst.forEach(System.out::print);
        lst.sort(byName);
        System.out.println("By Name:  ");
        lst.forEach(System.out::print);
        lst.sort(byId);
        System.out.println("By Id:  ");
        lst.forEach(System.out::print);
        }

    }



